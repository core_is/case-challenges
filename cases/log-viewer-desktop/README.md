# Visualizador de Logs

Objetivo deste teste é avaliar seus conhecimentos na linguagem de programação C++ e ambiente de desenvovimento Qt, também serão avaliados a organização, estilo e boas práticas de programação.

## Ambiente de desenvolvimento

* Qt 5.15.2 (<https://www.qt.io/download-qt-installer>)
* Qt Creator 4.15.1 (incluído na instalação do Qt)
* MinGW 8.1.0 32-bit ou 64-bit (incluído na instalação do Qt)

## Proposta

Utilizando este repositório criar um visualizador de arquvivos de logs.

## Desenvolvimento

Clone este repositório em sua máquina de desenvolvimento. Faça commits relevantes. Vamos analisar as mensagens usadas nos commits assim como o conteúdo de cada commit. A idéia é que cada commit seja tratado como se fosse um Pull Request (PR). Os commits não podem ser muito grandes e nem ter código não relacionado com a mensagem.

Para submeter sua solução, compacte a pasta raiz e nos envie por e-mail. Caso prefira, você pode subir sua solução em um repositório privado e convidar o avaliador para visualizar (neste caso, nos pergunte qual o endereço de e-mail do avaliador).

Não exponha publicamente sua solução na internet.

### Exemplo e estrutura de uma linha do arquivo de log

	2021/04/07 15:01:02.329 W [Service Session] Kill any ghost applications left behind.
	DATA HORA SEVERIDADE [NOME DO SERVIÇO] TEXTO DO LOG

Poderão existir casos de log onde existirão sub-serviços:

	2021/04/07 15:03:06.963 C [Service Session] [MongoDb server] integrity error.
	DATA HORA SEVERIDADE [NOME DO SERVIÇO] [NOME DO SUB-SERVIÇO] TEXTO DO LOG

### Funcionalidades que deverão ser implementadas no visualizador de log

* Filtrar logs por data e hora usando um intervalo inicial e final
* Filtrar logs por severidade:
	* D : debug
	* W : warning
	* C : critical
	* F : fatal
	* S : system
* Filtrar logs por nome do serviço
* Filtrar logs por nome do sub-serviço
* Filtrar logs por conteúdo do texto do log
* Buscar de logs por texto livre (Ctrl+F)

Para os filtros a interface deverá mostrar somente as linhas de log correspondentes ao filtro utilizado.

Para a busca a interface deverá localizar o texto livre e mover o scroll da mesma para a linha da busca, a busca deverá funcionar também para lista de logs filtrados.

* Pontos extras:
	* Mostrar a contagem de linhas de log mostradas pelos filtros implementados acima
	* Poder comparar as semelhanças ou diferenças entre dois filtros (será necessário duplicar a visualização do log para aplicar dois filtros e mostrar as semelhanças ou diferenças)
	* Testes unitários e/ou de integração

## Estrutura do projeto

	/src

Esta é a pasta onde está os fontes e arquivos do projeto Qt, todos os arquivos criados deverão estar nesta pasta.

	/logs

Esta é a pasta onde estão os arquivos logs para utilização e teste nesse visualizador de logs.

## O que apreciamos

* Código limpo e bem organizado
* Princípios SOLID de POO (<https://medium.com/desenvolvendo-com-paixao/o-que-%C3%A9-solid-o-guia-completo-para-voc%C3%AA-entender-os-5-princ%C3%ADpios-da-poo-2b937b3fc530>

## Copyright
Copyright©2021, I.Systems. Todos os direitos reservados.

Projeto Qt baseado no *Application Example* (<https://doc.qt.io/qt-5/qtwidgets-mainwindows-application-example.html>).  